


class parent:

    def __init__(self):
        print("in parent")
        self.x=10

    def disp(self):
        print(self.x)

class child(parent):
    def __init__(self):
        print("in child")
        self.y=20

    def show(self):
        super().__init__()
        print(self.x)
        print(self.y)
       # disp()

obj=child()
obj.show()
obj.disp()
