import array

l=list(range(1,20))
arr=array.array('i',l)

for i in arr:
    print(i,end=" ")

print("\n")
arr1=arr[10:20]
for i in arr1:
    print(i,end=" ")
print("\n")
arr2=arr[10:-5]
for i in arr2:
    print(i,end=" ")

print("\n")

arr4=arr[::-1]
for i in arr4:
    print(i,end=" ")

print("\n")
index = arr.index(2)
res = arr[index]
print(index,res)
