

class myclass:
    def __init__(self,value):
        self.value1=value

    def show(self):
        print(self.value1)

    @property
    def value(self):
        return 2*self.value1

    @value.setter
    def value(self,value2):
        self.value1=value2
        

obj=myclass(10)
obj.value=55
print(obj.value)
obj.show()
print(obj.value)
