/*7) Take the size of the array from the user. Create two arrays of that size. Initialize all
second array elements as zero(0). For the first array take all elements from the user.
Check if the elements in the first array are even or not if it's even then replace the
value of the second array of that index with 1 and print both the array.
5 pts.
Input : Size : 10
Array 1 Elements : 4 2 3 6 8 7 1 0 9 5
Output : Array 1 Elements : 4 2 3 6 8 7 1 0 9 5
Array 2 Elements : 1 1 0 1 1 0 0 1 0 0
*/

class arr{
	public static void main(String []tohid){
		

		int arr1[]=new int[]{4,2,3,6,8,7,1,0,9,5};
		int arr2[]=new int[arr1.length];

		for(int i=0;i<arr1.length;i++){
			if(arr1[i]%2==0){
				arr2[i] = 1;
				//System.out.print(arr2[i]);
			}
			System.out.print(arr2[i]);
		}
	}
}



