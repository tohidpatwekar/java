/*Que 5: WAP to toggle the String to uppercase or lowercase
Input: Java output: jAVA
Input: data output: DATA
*/

class str{
	public static void main(String []tohid){

		String str= "jaVaC@";
		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){
			if(arr[i]>='a' && arr[i]<='z'){
				arr[i] -= 32;
			}
			else if(arr[i]>='A' && arr[i]<='Z'){
				arr[i]+=32;
			}
			System.out.print(arr[i]);
		}
	}
}
