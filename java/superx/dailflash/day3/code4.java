/*Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
*/

class revrange{
	public static void main(String []tohid){

		int start=25435;
		int end=25449;


		for(int i=start;i<=end;i++){
			int num=i;
			int rem=0;
			int rev=0;
			while(num!=0){
				rem=num%10;
				rev=(rev*10)+rem;
				num=num/10;
			}
			System.out.println(rev);
		}
	}
}
