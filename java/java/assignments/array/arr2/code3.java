/*
Program 3
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26
*/
import java.io.*;
class Sumarr{
        public static void main(String []tohid)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of arr");
                int size = Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                int count1=0;
                int count2=0;
		int sum1=0;
		int sum2=0;

                System.out.println("Enter arr ele");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                for(int j=0;j<arr.length;j++){
                        if(arr[j]%2==0){
                               sum1=sum1+arr[j];
                        }
                        else{
                                sum2=sum2+arr[j];
                        }
                }
                System.out.println("Even Elements sum :  "+sum1);
                 System.out.println(" odd Elements sum :  "+sum2);
        }

}
