/*
Program 2
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3
*/

import java.io.*;
class Sumarr{
	public static void main(String []tohid)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of arr");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		int count1=0;
		int count2=0;

		System.out.println("Enter arr ele");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int j=0;j<arr.length;j++){
                        if(arr[j]%2==0){
				count1++;
			}
			else{
				count2++;
			}
                }
		System.out.println("Number of Even Elements:  "+count1);
		 System.out.println("Number of Even Elements:  "+count2);
	}

}

