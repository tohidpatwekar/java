/*
 
Q2.
Write a program in which ask the user to enter a number from 0 to 5 and print it's spelling,if the
entered number is greater than 5 print entered number is greater than 5
e.g
Input -Enter a number - 4
Output - four

*/
import java.io.*;

class SwitchDemo {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int num = Integer.parseInt(br.readLine());

		switch(num){

			case 1 :
				System.out.println("One\n");
				break;
			case 2 :
				System.out.println("Two\n");
				break;
			case 3 :
				System.out.println("Three\n");
				break;
			case 4 :
				System.out.println("Four\n");
				break;
			case 5 :
				System.out.println("Five\n");
				break;
			default :
				System.out.println("Number is greater than 5\n");
				break;
		}
	}
}


