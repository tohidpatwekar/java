/*
Q4
Take choice from user
Show this to user
What's your interest?
1.movies
2.Series
Enter your choice 1 or 2 :
If user enters 1 :
Movie you wish to watch today
1.Founder
2. Snowden
3.Jobs
4.Her
5.Social Network
6.Wall-E
7.AI
Enter your choice :
2 : Snowden
If user enters 2 :
Best series to watch
1.Silicon valley
2.Devs
3.the IT crowd
4.Mr Robot
Print users choice

*/
import java.io.*;

class SwitchDemo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Whats ur interest\n1.Movies\n2.Series");

                int num1 = Integer.parseInt(br.readLine());

		switch(num1){

			case 1 : 
				System.out.println("Movie you wish to watch today");
				System.out.println("1.Founder\n2. Snowden\n3.Jobs\n4.Her\n5.Social Network\n6.Wall-E\n7.AI");
				int num2 = Integer.parseInt(br.readLine());

				switch(num2){

					case 1 :
						System.out.println("1.Founder");
						break;
					case 2 :
						System.out.println("2.Snowden");
						break;
					case 3 :
						System.out.println("3.Jobs");
						break;
					case 4 :
						System.out.println("4.Her");
						break;
					case 5 :
						System.out.println("5.Social Network");
						break;
					case 6 :
						System.out.println("6.Wall-E");
						break;
					case 7 :
						System.out.println("7.AI");
						break;
				}
			break;

			case 2 :
				System.out.println("Best series to watch");
				System.out.println("1.Silicon valley\n2.Devs\n3.the IT crowd\n4.Mr Robot");
				int num3 = Integer.parseInt(br.readLine());

				switch(num3){

					case 1 : 
						System.out.println("1.Silicon valley");
						break;
					case 2 : 
						System.out.println("2.Devs");
						break;
					case 3 : 
						System.out.println("3.the IT crowd");
						break;
					case 4 : 
						System.out.println("4.Mr Robot");
						break;
				}
			break;
		}
	}
}
                

