/*

Q1
Write a program in which students should enter marks of 5 different subjects. If all subject
having above passing marks add them and provide to switch case to print grades(first class
second class), if student get fail in any subject program should print “You failed in exam”

*/

import java.io.*;

class SwitchDemo {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter subject 1 marks");
		int sub1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter subject 2 marks");
		int sub2 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter subject 3 marks");
		int sub3 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter subject 4 marks");
		int sub4 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter subject 5 marks");
		int sub5 = Integer.parseInt(br.readLine());

		int sum = sub1 + sub2 + sub3 + sub4 + sub5;
		int sum1 = sum * 100;
		int sum2 = sum1 / 500;

		//System.out.println(sum2);

		int sum3 = sum2 / 10;
		//System.out.println(sum3);


		switch(sum3){
			case 7 :
				System.out.println("First class with distinction\n");
				break;

			case 6 :				
				System.out.println("First class\n");
				break;

			case 5 :
				System.out.println("Higher second class\n");
				break;

			case 4 :
				System.out.println("Second class\n");
				break;

			default : 
				System.out.println("You are failed\n");
				break;
		}		


	}
}
