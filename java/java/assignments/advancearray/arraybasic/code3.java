/*3] Largest Element in Array
Given an array A[] of size n. The task is to find the largest element in it.
Example 1:
Input:
n = 5
A[] = {1, 8, 7, 56, 90}
Output: 90
Explanation:
The largest element of a given array is 90.
*/


class large{
	public static void main(String []tohid){

		int arr[]={111,8,7,56,90};

		int max=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max=arr[i];
			}
		}
		System.out.println(max);
	}
}
