/*Q9
Write a program to take a number as input and print the Addition of Factorials of each
digit from that number.
Input: 1234
Output: Addition of factorials of each digit from 1234 = 3
*/

class factdigit{
	public static void main(String[]tohid){
		int num=1234;
		int temp=num;
		int sum=0;
		while(temp!=0){
			int rem=temp%10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			temp=temp/10;
		}System.out.println("Addition of factorials of each digit="+sum);
	}
}

