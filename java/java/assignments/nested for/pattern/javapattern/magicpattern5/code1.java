/*
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
*/
import java.io.*;
class pattern{
	public static void main(String []args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter no of rows");
                int row= Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int num1=1;

			int num2=4;
			char ch1='A';
			char ch2='D';
			for(int j=1;j<=4;j++){

				if(i%2!=0){
					System.out.print(" "+ch2 +num2);
					ch2--;
					num2--;
				}
				else{
					System.out.print(" " +ch1 +num1);
				       	ch1++;
					num1++;
				}
			}System.out.println();
		}
	}
}


