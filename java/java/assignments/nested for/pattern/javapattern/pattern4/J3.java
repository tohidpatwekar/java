class NestedFor{

        public static void main(String[] args){

                int N = 4;
                int num = N*(N+1)/2;

                for(int i = 1 ; i <= N ; i++){

                        for(int j = 1 ; j <= i ; j++){

                                System.out.print(num-- +"  ");
                        }
                        System.out.println();
                        num++;
                }
        }
}
