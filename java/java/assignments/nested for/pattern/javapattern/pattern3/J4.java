class NestedFor{

        public static void main(String[] args){

                int N = 4;

                for(int i = 1 ; i <= N ; i++){

                        for(int j = 1 ; j <= N-i+1 ; j++){

                                System.out.print("3C  ");
                        }
                        System.out.println();
                }
        }
}
/*

Q4 write a program to print the following pattern

3C 3C 3C 3C
3C 3C 3C
3C 3C
3C

	USE THIS FOR LOOP STRICTLY for the outer loop
	
	for(int i =1;i<=4;i++){
	
	}

*/
