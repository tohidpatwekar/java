class NestedFor{

        public static void main(String[] args){

                int N = 4;
		int num = N*(N+1)/2;

                for(int i = 1 ; i <= N ; i++){

                        for(int j = 1 ; j <= N-i+1 ; j++){

                                System.out.print(num +"  ");
                        }
                        System.out.println();
			num++;
                }
        }
}

/*

Q5 write a program to print the following pattern

10 10 10 10
11 11 11
12 12
13

	USE THIS FOR LOOP STRICTLY for the outer loop
	
	for(int i =1;i<=4;i++){
	
	}

*/
