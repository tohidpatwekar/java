/*
Program 4: Write a program to count the Odd digits of the given number.
Input: 942111423
Output: count of odd digits = 4
*/

class odd{
	public static void main(String []tohid){
		int num=917204;
		int count=0;
		while(num!=0){
			int rem=num%10;

			if(rem%2!=0){
				count++;
			}
			num=num/10;
		}System.out.println(count);
	}
}
