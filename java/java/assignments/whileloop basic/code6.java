/*
Program 6: Write a program to print the sum of all even numbers and
multiplication of odd numbers between 1 to 10.
Output: sum of even numbers between 1 to 10 = 30
Multiplication of odd numbers between 1 to 10 = 945
*/

class sum{
	public static void main(String []tohid){
		int sum=0;
		int mult=1;
		for(int i=1;i<=10;i++){
			if(i%2==0){
				sum=sum+i;
			}
			else{
				mult=mult*i;
			}
		}
		System.out.println(" sum of even numbers between 1 to 10 =" +sum);
		System.out.println("Multiplication of odd numbers between 1 to 10 =" +mult);
	}

}
