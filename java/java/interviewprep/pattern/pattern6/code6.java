/*6. Take no of rows from the user ROWS = 4
      d
    C C C
  b b b b b
A A A A A A A
*/

class pattern{
	public static void main(String []tohid){
		char ch1='d';
		char ch2='D';

		for(int i=1;i<=4;i++){
			for(int sp=4;sp>i;sp--){
				System.out.print("  ");
			}
			for(int j=1;j<=2*i-1;j++){
				if(i%2!=0){
					System.out.print(ch1+" ");
				}
				else{
					System.out.print(ch2+" ");
				}
			}
			ch1--;
			ch2--;
			System.out.println();
		
		}
	}
}
