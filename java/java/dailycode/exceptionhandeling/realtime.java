

import java.util.Scanner;

class underflowException extends RuntimeException{
	underflowException(String msg){
		super(msg);	
	}
}
class overflowException extends RuntimeException{
	overflowException(String msg){
		super(msg);
		
	}
}

class client{

	public static void main(String []tohid)throws RuntimeException{
		int arr[]=new int[5];

		Scanner sc =new Scanner(System.in);
		
		System.out.println("enter value in array");

		for(int i=0;i<arr.length;i++){
			int data = sc.nextInt();

			if(data<0)
				throw new underflowException("less value");
			if(data>100)
				throw new overflowException("high value");
			arr[i]=data;
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}
}


