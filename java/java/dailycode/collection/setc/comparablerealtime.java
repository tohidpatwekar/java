import java.util.*;

class langdemo implements Comparable{

	int year=0;
	String lang =null;


	langdemo(int year,String lang){
		this.year=year;
		this.lang=lang;
	}

	public int compareTo(Object obj){
		return (lang).compareTo(((langdemo)obj).lang);
		

	}
	public String toString(){
		return "("+lang+","+ year +")";
	}

}

class user{
	public static void main(String []tohid){

		TreeSet ts = new TreeSet();

		ts.add(new langdemo(1995,"JAVA"));
		ts.add(new langdemo(1972,"C"));
		ts.add(new langdemo(1979,"CPP"));

		System.out.println(ts);
	}
}
